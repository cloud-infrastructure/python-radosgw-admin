%global 	pypi_name	radosgw-admin
%global		desc		Python REST API for the Ceph RADOS Gateway (radosgw) admin operations\
http://docs.ceph.com/docs/master/radosgw/adminops/

Name:           python-radosgw-admin
Version:        1.7.1
Release:        7%{?dist}
Summary:        Python REST API for the Ceph RADOS Gateway (radosgw) admin operations

License:        ASL 2.0
BuildArch:      noarch
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  python3-setuptools
BuildRequires:  python3-devel
BuildRequires:  python3-boto

%description
%{desc}

%package -n python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3-setuptools
Requires:       python3-boto

%description -n python3-%{pypi_name}
%{desc}

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%{python3_sitelib}/radosgw
%{python3_sitelib}/*.egg-info

%changelog
* Mon Nov 06 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 1.7.1-7
- Remove builds on centos

* Mon Jul 17 2023 Domingo Rivera Barros <driverab@cern.ch> 1.7.1-6
- Build for 9

* Thu Feb 09 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 1.7.1-5
- Rebuild for rhel8 and alma8

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.7.1-4
- Handle empty API response on quota_set

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.7.1-3
- Initial version for el8s

* Tue Jul 07 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.7.1-2
- Fix quota set when radosgw returns an empty content

* Tue May 05 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.7.1-1
- Initial rebuild for el8

* Fri Jan 11 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-1
- Add create_key and remove_key actions
- Rebase package from upstream

* Wed Oct 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.3-2
- Send only quota parameters with values

* Wed Oct 24 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.3-1
- Add port and insecure options to example script
- Add quota management for user/bucket

* Mon Oct 15 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.3-0
- Initial version of the package
